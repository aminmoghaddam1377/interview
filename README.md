### Summary
This is a golang webapplication uses an apiKey to get connected to the newsapi.org to show you all newses and give the capability of searching trough panel.

### Uses
* [News API](https://newsapi.org/)
* Go Template & Go Http package

#### Usage
<pre>git clone https://gitlab.com/aminmoghaddam1377/interview.git
go build ./ -o gowebapp
export API_KEY="asasasxxxxx"
export PORT="8090"
./gowebapp
</pre>


#### Usage on Gitlab CI
This application has four different stages on it's ci. <br />
1. unit test of the application which will test a sample function. 
2. code quality and security check of the application
3. building the application and pushing the application in Dockerhub repository
4. changing the application kubernetes object ( manifests ) inside another repository called gitlab.com/aminmoghaddam1377/gowebapp_manifests.git. After chaing the manifests it will create a new commit the argoCD will be notified by Gitlab webhook to read the new changes and sync the application in kubernetes cluster.
Required Gitlab CI Variables:
```
ACCESS_TOKEN=<access token of the destination gitlab manifest repo for pushing and commit changes>
DOCKER_REPO_ADDRESS=<myrepo/gowebapp>
DOCKER_REPO_PASS=<docker repo password for private repos>
DOCKER_REPO_USER=<docker repo username>
```

