package main

import (
	"fmt"
	"io"
	"net/http"
	"testing"
)

func TestMetricHandler(t *testing.T) {
	panicFunc := func(t testing.TB, template string, output string) {
		t.Helper()
		if template != output {
			t.Errorf("the http response result is different that provided template!!!\n Got %s\n Want: %s\n", template, output)
		}
	}
	Metrics.ReqCounter = 50
	Metrics.SearchCounter = 50
	go func() {
		http.HandleFunc("/metrics", metricHandler)
		http.ListenAndServe(":65500", nil)
	}()

	httpReq, err := http.NewRequest("GET", "http://localhost:65500/metrics", nil)
	if err != nil {
		t.Error(err)
	}
	httpClient := &http.Client{}
	httpRes, err := httpClient.Do(httpReq)
	if err != nil {
		t.Error(err)
	}
	if httpRes.StatusCode != http.StatusOK {
		if err != nil {
			t.Error(err)
		}
	}
	byteSlice, _ := io.ReadAll(httpRes.Body)
	want := fmt.Sprintf("http_requests_total{code=\"200\",path=\"/\",method=\"GET\"} 50\nhttp_search_total{code=\"200\",path=\"/search\",method=\"GET\"} 50\n")
	got := string(byteSlice)
	panicFunc(t, want, got)
}
