package main

import (
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"net/url"
	"os"
	"strconv"
)

// Source Structure
type Source struct {
	ID   interface{} `json:"id"`
	Name string      `json:"name"`
}

// Article Structure
type Article struct {
	Source      Source `json:"source"`
	Author      string `json:"author"`
	Title       string `json:"title"`
	Description string `json:"description"`
	URL         string `json:"url"`
	URLToImage  string `json:"urlToImage"`
	PublishedAt string `json:"publishedAt"`
	Content     string `json:"content"`
}

// Results Structure
type Results struct {
	Status       string    `json:"status"`
	TotalResults int       `json:"totalResults"`
	Articles     []Article `json:"articles"`
}

// Search Query Structure
type Search struct {
	SearchKey  string
	NextPage   int
	TotalPages int
	Results    Results
}

type HttpMetrics struct {
	ReqCounter    int
	SearchCounter int
}

// IsLastPage checker
func (s *Search) IsLastPage() bool {
	return s.NextPage >= s.TotalPages
}

// CurrentPage Checker
func (s *Search) CurrentPage() int {
	if s.NextPage == 1 {
		return s.NextPage
	}

	return s.NextPage - 1
}

// PreviousPage checker
func (s *Search) PreviousPage() int {
	return s.CurrentPage() - 1
}

var tpl = template.Must(template.ParseFiles("index.html"))

var MetricTpl = `http_requests_total{code="200",path="/",method="GET"} {{.ReqCounter}}
http_search_total{code="200",path="/search",method="GET"} {{.SearchCounter}}
`
var apiKey string
var Metrics HttpMetrics = HttpMetrics{
	ReqCounter:    0,
	SearchCounter: 0,
}

func indexHandler(w http.ResponseWriter, r *http.Request) {
	_ = tpl.Execute(w, nil)
	Metrics.ReqCounter += 1
}

// Search Requests
func searchHandler(w http.ResponseWriter, r *http.Request) {

	u, err := url.Parse(r.URL.String())
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		_, err := w.Write([]byte("Internal server error"))
		if err != nil {
			log.Println(err)
		}
		return
	}

	params := u.Query()
	searchKey := params.Get("q")
	page := params.Get("page")
	if page == "" {
		page = "1"
	}

	fmt.Printf("Search Query is: %s\n", searchKey)
	// fmt.Println("Results page is: ", page)
	search := &Search{}
	search.SearchKey = searchKey

	next, err := strconv.Atoi(page)
	if err != nil {
		http.Error(w, "Unexpected server error", http.StatusInternalServerError)
		return
	}

	search.NextPage = next
	pageSize := 20

	endpoint := fmt.Sprintf("https://newsapi.org/v2/everything?q=%s&pageSize=%d&page=%d&apiKey=%s&sortBy=publishedAt&language=en", url.QueryEscape(search.SearchKey), pageSize, search.NextPage, apiKey)

	resp, err := http.Get(endpoint)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	err = json.NewDecoder(resp.Body).Decode(&search.Results)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	search.TotalPages = int(float64(search.Results.TotalResults / pageSize))
	err = tpl.Execute(w, search)
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
	}

	search.TotalPages = int(float64(search.Results.TotalResults / pageSize))

	if ok := !search.IsLastPage(); ok {
		search.NextPage++
	}
	Metrics.SearchCounter += 1
}

func metricHandler(w http.ResponseWriter, r *http.Request) {
	newTpl := template.New("Metric Template")
	parsedTpl, err := newTpl.Parse(MetricTpl)
	if err != nil {
		log.Println(parsedTpl)
	}
	_ = parsedTpl.Execute(w, Metrics)
}

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	apiKey = os.Getenv("API_KEY")
	// println(apiKey)
	// println(os.Getenv("API_KEY"))
	if apiKey == "" {
		log.Fatal("API_KEY must be set")
	}

	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}

	mux := http.NewServeMux()
	//
	//Static Files
	fs := http.FileServer(http.Dir("assets"))
	mux.Handle("/assets/", http.StripPrefix("/assets/", fs))

	//Server Start
	fmt.Println("Starting Server...")
	mux.HandleFunc("/search", searchHandler)
	mux.HandleFunc("/", indexHandler)
	mux.HandleFunc("/metrics", metricHandler)
	err := http.ListenAndServe(":"+port, mux)
	if err != nil {
		log.Println(err)

	}

}
